#include <gtest/gtest.h>
#include <pangram.h>

using namespace std;

// My own tests

string fullAlphabet = "abcdefghijklmnopqrstuvwxyz";

TEST(PangramTest, FullAlphabetIsPangram) {
  PangramChecker c(fullAlphabet);
  EXPECT_EQ(true, c.isPangram());
  EXPECT_EQ("NULL", c.getMissingCharacters());
}

TEST(PangramTest, IgnoresCase) {
  PangramChecker c("aBcDeFgHiJkLmNoPqRsTuVwXyZ");
  EXPECT_EQ(true, c.isPangram());
  EXPECT_EQ("NULL", c.getMissingCharacters());
}

TEST(PangramTest, FullAlphabetMissingForEmptyString) {
  PangramChecker c("");
  EXPECT_EQ(false, c.isPangram());
  EXPECT_EQ(fullAlphabet, c.getMissingCharacters());
}

TEST(PangramTest, IgnoresNonLetterCharacters) {
  PangramChecker c(".;'");
  EXPECT_EQ(false, c.isPangram());
  EXPECT_EQ(fullAlphabet, c.getMissingCharacters());
}

TEST(PangramTest, IgnoresNonAsciiCharacters) {
  PangramChecker c("Ąźć");
  EXPECT_EQ(false, c.isPangram());
  EXPECT_EQ(fullAlphabet, c.getMissingCharacters());
}

// Tests provided by Golden Frog

TEST(PangramTest, QuickBrownIsPangram) {
  PangramChecker c("A quick brown fox jumps over the lazy dog.");
  EXPECT_EQ(true, c.isPangram());
  EXPECT_EQ("NULL", c.getMissingCharacters());
}

TEST(PangramTest, SlowYellowIsNotPangram) {
  PangramChecker c("A slow yellow fox crawls under the proactive dog.");
  EXPECT_EQ(false, c.isPangram());
  EXPECT_EQ("bjkmqz", c.getMissingCharacters());
}

