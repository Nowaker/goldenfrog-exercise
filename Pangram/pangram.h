#include <set>
#include <string>

class PangramChecker {
public:
  PangramChecker(std::string checkedString);
  bool isPangram();
  std::string getMissingCharacters();
private:
  std::string checkedString;
  bool pangram = false;
  std::string missingCharacters;
};
