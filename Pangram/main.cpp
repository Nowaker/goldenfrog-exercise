#include "pangram.h"
#include <iostream>

int main() {
  std::string input;
  while(std::getline(std::cin, input)) {
    PangramChecker checker(input);
    std::cout << checker.getMissingCharacters() << std::endl;
  }
  return 0;
}
