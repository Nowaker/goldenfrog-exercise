#include "pangram.h"
#include <algorithm>

PangramChecker::PangramChecker(std::string string) : checkedString(string) {
  static auto alphabetChars = []() {
    std::set<char> chars;
    for (char i = 'a'; i <= 'z'; i++) {
      chars.insert(i);
    }
    return chars;
  }();

  std::set<char> checkedChars;
  for(char c : checkedString) {
    if ((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z')) {
      checkedChars.insert((char) std::tolower(c));
    }
  }

  if (alphabetChars == checkedChars) {
    pangram = true;
    missingCharacters = "NULL";
    return;
  }

  std::set<char> missingChars;
  std::set_difference(alphabetChars.begin(), alphabetChars.end(), checkedChars.begin(), checkedChars.end(),
      std::inserter(missingChars, missingChars.end()));

  pangram = false;
  for (char c : missingChars) {
    missingCharacters += c;
  }
}

bool PangramChecker::isPangram() {
  return pangram;
}

std::string PangramChecker::getMissingCharacters() {
  return missingCharacters;
}
