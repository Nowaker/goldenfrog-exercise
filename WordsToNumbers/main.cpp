#include "words_to_numbers.h"
#include <iostream>

int main() {
  std::string words;
  while(std::getline(std::cin, words)) {
    try {
      auto numbers = wordsToNumbers(words);
      std::cout << numbers << std::endl;
    } catch (std::out_of_range) {
      std::cout << "Invalid input." << std::endl;
    }
  }
  return 0;
}
