#include "words_to_numbers.h"
#include <boost/algorithm/string.hpp>
#include <map>
#include <iostream>

std::string wordsToNumbers(std::string words) {
  if (words.empty()) {
    return "";
  }

  std::vector<std::string> wordsVec;
  boost::split(wordsVec, words, boost::is_any_of(";"));
  return wordsToNumbers(wordsVec);
}

std::string wordsToNumbers(std::vector<std::string> words) {
  static std::map<std::string, std::string> numbersMap = {
      {"one", "1"}, {"two", "2"}, {"three", "3"}, {"four", "4"}, {"five", "5"},
      {"six", "6"}, {"seven", "7"}, {"eight", "8"}, {"nine", "9"}, {"zero", "0"}
  };

  std::vector<std::string> numbersVec;
  std::transform(words.begin(), words.end(), std::back_inserter(numbersVec), [](std::string word) -> std::string {
    return numbersMap.at(word);
  });
  return boost::join(numbersVec, "");
}
