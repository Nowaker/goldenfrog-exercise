class Player
  def initialize name
    @name = name
  end

  def to_s
    "Player: #{@name}"
  end
end

class Coords
  attr_reader :x, :y

  def initialize x, y
    validate! x
    validate! y
    @x, @y = x, y
  end

  # This let's us create a new Coords object like this: Coords[1, 2] instead of this: Coords.new(1, 2)
  def self.[] x, y
    Coords.new x, y
  end

  def to_s
    "[#{@x},#{@y}]"
  end

  def inspect
    to_s
  end

  def == other
    x == other.x && y == other.y
  end

  private

  def validate! coord
    raise "Coords requires an Integer, given: #{coord.class.name}" unless coord.is_a? Integer
    raise "Coords must be 0..#{Board::BOARD_SIZE}, given: #{coord}" if coord < 0
    raise "Coords must be 0..#{Board::BOARD_SIZE}, given: #{coord}" if coord >= Board::BOARD_SIZE
  end
end

class Board
  BOARD_SIZE = 8

  def initialize white_player, black_player
    @white = white_player
    @black = black_player
    @fields = Array.new(BOARD_SIZE) { Array.new(BOARD_SIZE) } # No magic numbers
  end

  def setup!
    # Initial setup is described here:
    # https://upload.wikimedia.org/wikipedia/commons/thumb/d/d7/Chessboard480.svg/264px-Chessboard480.svg.png
    # 1-a is [0][0], 1-h is [0][8], 8-a is [7][0], 8-h is [7][7]

    # This definition of initial setup is pure code. We could store it as YAML configuration instead.
    place_piece King, Coords[0, 4]
    place_piece Queen, Coords[0, 3]
    place_piece Rook, Coords[0, 0], Coords[0, 7]
    place_piece Bishop, Coords[0, 2], Coords[0, 5]
    place_piece Knight, Coords[0, 1], Coords[0, 6]

    pawns_coords = (0..7).map { |n| Coords[1, n] }
    place_piece Pawn, *pawns_coords
  end

  def make_move player, from, to
    # Algorithm to determine if move is valid:
    # 1. Board: make sure there's a piece on the board in "from" and it belongs to the current player.
    # 2. Board: make sure "from" and "to" aren't the same.
    # 3. Board: make sure there's no piece of same color on "to" field.
    # 4. Piece: make sure a piece can move to a new position by the rules.
    #    For example, Pawn can advance by 1 or 2 forward. King can advance 1 in any direction.
    #    But neither can go beyond the border.
    # 5. Board: eliminate forbidden positions for a King.
    # If all looks good:
    # 1. Make the move on the Board.
    # 2. Check if game over.

    piece = @fields[from.x][from.y]
    if piece.nil?
      raise 'Move invalid: no piece here'
    end

    if from == to
      raise "Move invalid: you can't remain in place"
    end

    piece_at_new_field = @fields[to.x][to.y]

    unless piece.move_valid? from, to, @fields
      raise "Move invalid: #{piece} won't move from #{from} to #{to}"
    end

    if piece_at_new_field && piece_at_new_field.color == piece.color
      raise "Move invalid: #{piece} can't move to an occupied field"
    end

    if piece.is_a? King
      endangered = false # TODO: Check if move would endanger king
      raise "Move invalid: move would endanger king" if endangered
    end

    @fields[to.x][to.y] = piece
    @fields[from.x][from.y] = nil

    unless piece.is_a? King
      opponents_king_can_move = true # TODO: Check if opposite king can move
      raise "Game over! #{player} wins." unless opponents_king_can_move
    end
  end

  private

  def place_piece piece_class, *multiple_coords
    multiple_coords.each do |coords|
      # Because setup is identical on each side of the board, we need to transform y.
      # For example: 1-a (0,0) -> 8-a (7,0), 2-d (1, 3)-> 7-d (6,3)
      @fields[coords.x][coords.y] = piece_class.new @white, White.new
      @fields[BOARD_SIZE - 1 - coords.x][coords.y] = piece_class.new @black, Black.new
    end
  end

  def to_s
    s = StringIO.new
    coords = (0...BOARD_SIZE)
    coords.to_a.reverse.each do |x|
      s << x
      coords.each do |y|
        piece = @fields[x][y]
        # Possible improvement: instead of having a Board array that consists of Pieces or nils, we could have a Board
        # that consists of BoardFields which in turn hold a Piece or not, and are responsible for formatting
        if piece.nil?
          s << ' '
        else
          s << piece.to_s
        end
      end
      s << "\n"
    end
    s << ' ' << coords.map(&:to_s).join << "\n"
    s.string
  end
end

class Color
end

# I could use a symbol or string instead of a class. Initially, that would work fine and 'white' would print as such
# in the console. However, if I wanted to change the way it prints, I'd have to find all occurrences of 'white' string
# in the code and replace it with 'my_new_white'. However, that would break previous saved games. For this reason, I
# prefer a class. I would simply introduce a new method `color_name` and keep the old class name as an identifier for
# serialization. And if I wanted to change the class name as well to match the new color name, I would introduce two
# methods: `color_name` and `identifier`. With classes, refactoring ain't a problem.
class White < Color
  def advance_by
    1
  end
end

class Black < Color
  def advance_by
    -1
  end
end

class Piece
  attr_reader :player, :color

  def initialize player, color
    @player, @color = player, color
    raise 'Allowed colors: White.new, Black.new' unless color.is_a? Color
  end

  def to_s
    case @color
    when White
      symbols[0]
    when Black
      symbols[1]
    else
      raise "Unrecognized color: #{@color}"
    end
  end
end

class King < Piece
  def symbols
    %w(♔ ♚)
  end

  def move_valid? from, to, fields
    raise # TODO
  end
end

class Queen < Piece
  def symbols
    %w(♕ ♛)
  end

  def move_valid? from, to, fields
    x_advance = to.x - from.x
    y_advance = to.y - from.y

    return false unless nothing_in_way? from, to, fields
    return true if x_advance.abs == y_advance.abs
    return true if x_advance != 0 && y_advance == 0
    return true if x_advance == 0 && y_advance != 0

    false
  end

  def nothing_in_way? from, to, fields
    # Queen can't jump over other pieces.
    # TODO: Iterate through each advance field one by one and check if empty.
    true
  end
end

class Rook < Piece
  def symbols
    %w(♖ ♜)
  end

  def move_valid? from, to, fields
    raise # TODO
  end
end

class Bishop < Piece
  def symbols
    %w(♗ ♝)
  end

  def move_valid? from, to, fields
    raise # TODO
  end
end

class Knight < Piece
  def symbols
    %w(♘ ♞)
  end

  def move_valid? from, to, fields
    raise # TODO
  end
end

class Pawn < Piece
  def symbols
    %w(♙ ♟)
  end

  def move_valid? from, to, fields
    piece_at_new_field = fields[to.x][to.y]

    # X and Y advance is calculated in many places. We could introduce a class Move that keeps "from" and "to", and
    # calculated X and Y advance.
    x_advance = to.x - from.x
    y_advance = to.y - from.y

    # This could be improved by introducing a relative "forward". For whites, forward is +1 in coords. For blacks,
    # forward is -1. For whites, pawns can go by 2 units when starting from [1,*], while for blacks it's [6,*].
    if @color.is_a? White
      return true if from.x == 1 && x_advance == 2 && y_advance == 0
      return true if x_advance == 1 && y_advance == 0
      return true if x_advance == 1 && y_advance.abs == 1 && piece_at_new_field
    elsif @color.is_a? Black
      return true if from.x == 6 && x_advance == -2 && y_advance == 0
      return true if x_advance == -1 && y_advance == 0
      return true if x_advance == -1 && y_advance.abs == 1 && piece_at_new_field
    end

    false
  end
end
