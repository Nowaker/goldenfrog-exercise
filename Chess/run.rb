#!/usr/bin/env ruby

require_relative 'board'

white = Player.new 'nowaker'
black = Player.new 'rush'

board = Board.new white, black
board.setup!
puts board
puts

board.make_move white, Coords[1, 4], Coords[3, 4]
puts board
puts

board.make_move black, Coords[6, 3], Coords[4, 3]
puts board
puts

board.make_move white, Coords[3, 4], Coords[4, 3]
puts board
puts

board.make_move white, Coords[7, 3], Coords[4, 3]
puts board
puts

exit

# Here's how a game loop could look like.

until board.game_over?
  # Each player makes a move. It could be done via UI (drag and drop) or console (e.g. input like "2B 3B").
  # We don't care how it's done here, it's an implementation detail of `acquire_move` method that returns a Move object.
  player, from, to = ui.acquire_move
  board.make_move player, from, to
  ui.refresh
end

