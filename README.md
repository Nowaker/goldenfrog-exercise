# GOLDENFROG HIRING EXERCISES

## 1. PANGRAMS

The sentence “A quick brown fox jumps over the lazy dog” contains every single letter in the
alphabet. Such sentences are called pangrams. You are to write a function, which takes a
sentence, and returns all the letters it is missing (which prevent it from being a pangram). You
should ignore the case of the letters in the sentence, and your return should be all lower case
letters, in alphabetical order. You should also ignore all non US-ASCII characters. In case the
input sentence is already a pangram, print out the string NULL

INPUT SAMPLE:

```
A quick brown fox jumps over the lazy dog.
A slow yellow fox crawls under the proactive dog.
```

OUTPUT SAMPLE:

```
NULL
bjkmqz
```

### Comments from Nowaker

1. Code is UTF-dumb. Each byte is considered a separate character.
   Second byte of multi-byte character can be misrecognized as ASCII character, e.g. 'a'.
   `std::wstring` could be used instead.

## 2. WORDS TO NUMBERS

Write a function that takes in a string of a collection of numbers represented in word form and
prints out the numbers in number form.

All numbers are separated by a semicolon. There are up to 20 numbers in one line. The numbers
are "zero" to "nine"

INPUT SAMPLE:

```
zero;two;five;seven;eight;four
three;seven;eight;nine;two
```

OUTPUT SAMPLE:

```
025784
37892
```

### Comments from Nowaker

1. The spec doesn't say anything about empty strings as input. I assumed an empty string shall be returned.
2. The spec doesn't specify the course of action for an unexpected input, e.g. `six;blah;seven`.
   Currently, `std::out_of_range` will be thrown. We could re-throw this as our own exception type,
   preserve unexpected token `6blah7`, or delete it `67`, depending on business requirements.
   I personally prefer exceptions to unexpected results.
   [Because JavaScript](http://dorey.github.io/JavaScript-Equality-Table/).
3. The spec reads "There are up to 20 numbers in one line". My code will handle any number of numbers in one line.
   However, if a limit of 20 was dictated by security (e.g. anti-DoS measure), I'd rewrite the `std::transform` part
   of the code to a standard for-each loop with counter and conditional break.
4. I used Boost to get rid of vector split/join boilerplate at the expense of extra 300 KB in binary size and
   potentially performance. This might or might not be justified depending on target hardware.

## 3. OBJECT ORIENTED PROGRAMMING

Using any language you prefer, create the basic classes you would need to make the game of
Chess (if you are inexperienced with Chess, please feel free to do a different game instead, like
Texas Hold ‘Em or Blackjack). Create the classes as best you can, with member variables,
functions, inheritance, etc. For the sake of time, the functions can have commented pseudo code
instead of actual code. Please, attach the code files to your reply email.

### Comments from Nowaker

Real code written in Ruby. Instructions to run the code:

```
bundle || gem install bundler
bundle install
./run.rb
```

