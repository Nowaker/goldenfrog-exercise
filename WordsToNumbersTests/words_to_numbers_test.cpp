#include <gtest/gtest.h>
#include <words_to_numbers.h>

using namespace std;

// My own tests

TEST(WordsToNumbersTest, NoWordsIsNoNumbers) {
  EXPECT_EQ("", wordsToNumbers(""));
}

TEST(WordsToNumbersTest, BadWordsIsException) {
  EXPECT_THROW(wordsToNumbers("six;badword;six"), std::out_of_range);
}

// Tests provided by Golden Frog

TEST(WordsToNumbersTest, FirstGoldenFrogExample) {
  EXPECT_EQ("025784", wordsToNumbers("zero;two;five;seven;eight;four"));
}

TEST(WordsToNumbersTest, SecondGoldenFrogExample) {
  EXPECT_EQ("37892", wordsToNumbers("three;seven;eight;nine;two"));
}
